import React, {useState, useEffect} from 'react';
import './Main.css';
import {getObjects} from "./GET/GetPage";
import {Link} from "react-router-dom";
import ModelElement from './items/ModelElement';
 
const randomArticles = (articles) => {
    const array = [...articles];
    const shuffleArray = (array) => {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }
 
    shuffleArray(array);
    return array.splice(0, 10);
}
 
function Index() {
    const [models, setModels] = useState(null);
    useEffect(() => {
        if (!models) {
            let globName = "188.124.37.185";
            let apiUrl = "/api/wiki";
            fetch("http://" + globName + ":5000" + apiUrl)
                .then(response => response.json())
                .then(response => getObjects(response))
                .then(data => {
                    const articles = randomArticles(data);
                    setModels(articles)
                });
        }
    }, [models]);
 
  return (
      <main>
        <h1>Hello! This is wiki home page. <br/></h1>
        <div>From here you can  <Link to="/get">view all</Link> wiki elements, <Link to="/post">create</Link> a new element or <Link to="/put">edit</Link> existing one.</div>
          <ul>
              {(models != null) && models.map(item => <li key={item._id}>
                      <ModelElement
                          visibleFields={[
                              "name",
                              "russian_description",
                              "description",
                              "media"
                          ]}
 
                          isButtonVisible={false}
                          model={item}/>
                  </li>
              )}
          </ul>
      </main>
  );
}
 
export default Index;